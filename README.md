# README #


### What is this repository for? ###

Create a consistent look and feel for Lightning web components by using a common CSS module.
Define styles in the CSS module, and import the module into the components that share those styles.

### How do I get set up? ###

* 1)Create a component that contains a CSS file and a configuration file. The folder and filenames must be identical.
* 2)In the Common css file write the common css code.
* 3)In the CSS file of the component import the module. Syntax for importing is :
* @import 'namespace/moduleName';
